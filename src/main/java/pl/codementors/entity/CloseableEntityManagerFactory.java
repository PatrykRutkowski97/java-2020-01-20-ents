package pl.codementors.entity;

import javax.persistence.EntityManagerFactory;

public class CloseableEntityManagerFactory implements AutoCloseable {

    private final EntityManagerFactory emf;

    public CloseableEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public CloseableEntityManager createEntityManager() {
        return new CloseableEntityManager(emf.createEntityManager());
    }

    @Override
    public void close() throws Exception {
        emf.close();
    }
}
