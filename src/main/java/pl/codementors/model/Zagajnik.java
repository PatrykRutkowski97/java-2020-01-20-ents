package pl.codementors.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
public class Zagajnik {

    @Id
    private String name;
    @ManyToOne
    private Ent ent;

    public Zagajnik() {
    }

    public Zagajnik(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ent getEnt() {
        return ent;
    }

    public void setEnt(Ent ent) {
        this.ent = ent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Zagajnik zagajnik = (Zagajnik) o;
        return Objects.equals(name, zagajnik.name) &&
                Objects.equals(ent, zagajnik.ent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, ent);
    }
}
