package pl.codementors.model;

import org.hibernate.annotations.Table;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Objects;

@Entity
public class Ent {

    private Type type;
    @Id
    private String name;
    private double height;
    @OneToMany(mappedBy = "ent")
    private List<Zagajnik> zagajnikList;

    public Ent() {
    }

    public Ent(Type type, String name, double height) {
        this.type = type;
        this.name = name;
        this.height = height;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public List<Zagajnik> getZagajnikList() {
        return zagajnikList;
    }

    public void setZagajnikList(List<Zagajnik> zagajnikList) {
        this.zagajnikList = zagajnikList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ent ent = (Ent) o;
        return Double.compare(ent.height, height) == 0 &&
                type == ent.type &&
                Objects.equals(name, ent.name) &&
                Objects.equals(zagajnikList, ent.zagajnikList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name, height, zagajnikList);
    }
}
