package pl.codementors.repository;

import pl.codementors.entity.CloseableEntityManager;
import pl.codementors.entity.CloseableEntityManagerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.function.Function;


public abstract class BaseRepository<T,E> {

    protected abstract Class<T> getEntityClass();

    protected abstract Function<T, E> getKeyExtractor();

    protected final CloseableEntityManagerFactory factory;

    protected BaseRepository(CloseableEntityManagerFactory factory) {
        this.factory = factory;
    }

    public T find(Integer id) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            return em.find(getEntityClass(), id);
        }
    }

    public void create(T entity) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
        }
    }

    public T update(T entity) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            em.getTransaction().begin();
            entity = em.merge(entity);
            em.getTransaction().commit();
            return entity;
        }
    }

    public void remove(T entity) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            em.getTransaction().begin();
            em.remove(em.find(getEntityClass(), getKeyExtractor().apply(entity)));
            em.getTransaction().commit();
        }
    }

    public List<T> findAll() {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<T> query = builder.createQuery(getEntityClass());
            Root<T> root = query.from(getEntityClass());
            query.select(root);
            return em.createQuery(query).getResultList();
        }
    }

    public List<T> findAll(int offset, int limit) {
        try (CloseableEntityManager em = factory.createEntityManager()) {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<T> query = builder.createQuery(getEntityClass());
            Root<T> root = query.from(getEntityClass());
            query.select(root);
            return em.createQuery(query)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();
        }
    }

}
