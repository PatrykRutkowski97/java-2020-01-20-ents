package pl.codementors.repository;

import pl.codementors.entity.CloseableEntityManagerFactory;
import pl.codementors.model.Ent;

import java.util.function.Function;

public class EntRepository extends BaseRepository<Ent, String> {


    protected EntRepository(CloseableEntityManagerFactory factory) {
        super(factory);
    }

    @Override
    protected Class<Ent> getEntityClass() {
        return Ent.class;
    }

    @Override
    protected Function<Ent, String> getKeyExtractor() {
        return Ent::getName;
    }
}
