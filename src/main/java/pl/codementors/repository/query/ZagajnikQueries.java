package pl.codementors.repository.query;

import pl.codementors.model.Zagajnik;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public interface ZagajnikQueries {

    TypedQuery<Zagajnik> findAllEmptyZagajnik(EntityManager em);

    TypedQuery<Zagajnik> findAllAuthorsWithBooksMoreThan(EntityManager em, long count);

    TypedQuery<Zagajnik> findAllAuthorsWithBooksCount(EntityManager em);

}
