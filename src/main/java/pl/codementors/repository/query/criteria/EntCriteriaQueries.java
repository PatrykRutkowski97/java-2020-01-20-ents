package pl.codementors.repository.query.criteria;

import pl.codementors.model.Ent;
import pl.codementors.repository.query.EntQueries;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class EntCriteriaQueries implements EntQueries {
    @Override
    public TypedQuery<Ent> findAllEnt(EntityManager em) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Ent> query = cb.createQuery(Ent.class);
        Root<Ent> root = query.from(Ent.class);
        query.select(root);
        return em.createQuery(query);

    }

    @Override
    public TypedQuery<Ent> findAllMoreThan(EntityManager em, long count) {

        return null;
    }
}
