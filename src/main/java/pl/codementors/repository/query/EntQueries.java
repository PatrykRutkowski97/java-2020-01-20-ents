package pl.codementors.repository.query;

import pl.codementors.model.Ent;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public interface EntQueries {

    TypedQuery<Ent> findAllEnt(EntityManager em);

    TypedQuery<Ent> findAllMoreThan(EntityManager em, long count);

}
